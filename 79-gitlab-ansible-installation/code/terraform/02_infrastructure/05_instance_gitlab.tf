module "gitlab" {
  source                      = "../modules/instance"
  instance_count              = 1
  instance_name               = "gitlab"
  instance_key_pair           = "default_key"
  instance_security_groups    = ["all_internal"]
  instance_network_internal   = var.network_internal_dev
  instance_ssh_key = var.ssh_public_key_default_user
  instance_flavor_name        = "a4-ram8-disk20-perf1"
  instance_default_user = var.default_user
  instance_volumes_count = 1
  metadatas                   = {
    environment          = "dev",
    app         = "gitlab"
  }
}
