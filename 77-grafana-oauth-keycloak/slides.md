%title: Infra Cloud Infomaniak
%author: xavki


██╗  ██╗███████╗██╗   ██╗ ██████╗██╗      ██████╗  █████╗ ██╗  ██╗
██║ ██╔╝██╔════╝╚██╗ ██╔╝██╔════╝██║     ██╔═══██╗██╔══██╗██║ ██╔╝
█████╔╝ █████╗   ╚████╔╝ ██║     ██║     ██║   ██║███████║█████╔╝ 
██╔═██╗ ██╔══╝    ╚██╔╝  ██║     ██║     ██║   ██║██╔══██║██╔═██╗ 
██║  ██╗███████╗   ██║   ╚██████╗███████╗╚██████╔╝██║  ██║██║  ██╗
╚═╝  ╚═╝╚══════╝   ╚═╝    ╚═════╝╚══════╝ ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝



-----------------------------------------------------------------------------------------------------------

# Grafana : Keycloak as OAuth

<br>

Purpose : https://grafana.com/docs/grafana/latest/setup-grafana/configure-security/configure-authentication/keycloak/

-----------------------------------------------------------------------------------------------------------

# Grafana : Keycloak as OAuth

<br>

- name: Add oauth to grafana.ini
  ansible.builtin.blockinfile:
    path: /etc/grafana/grafana.ini
    insertafter: ".*auth.generic_oauth.*"
    block: |
      tls_skip_verify_insecure = true
      enabled = true
      name = Keycloak-OAuth
      allow_sign_up = true
      client_id = {{ all_keycloak_client_id }}
      client_secret = {{ all_keycloak_client_secret }}
      scopes = openid email profile offline_access roles
      email_attribute_path = email
      login_attribute_path = username
      name_attribute_path = full_name
      auth_url = {{ all_keycloak_url }}/auth/realms/{{ all_keycloak_realm }}/protocol/openid-connect/auth
      token_url = {{ all_keycloak_url }}/auth/realms/{{ all_keycloak_realm }}/protocol/openid-connect/token
      api_url = {{ all_keycloak_url }}/auth/realms/{{ all_keycloak_realm }}/protocol/openid-connect/userinfo
      role_attribute_path = contains(roles[*], 'admin') && 'Admin' || contains(roles[*], 'editor') && 'Editor' || 'Viewer'
  notify: restart_grafana
  when: grafana_oauth_enabled: true

- name: change root_url to use sso
  lineinfile:
    path: /etc/grafana/grafana.ini
    regexp: "{{ item.before }}"
    line: "{{ item.after }}"
  with_items:
  - { before: "^;domain = ", after: "domain = {{ all_keycloak_client_url }}"}
  - { before: "^;root_url = admin", after: "root_url = https://{{ all_keycloak_client_url }}"}
  when: grafana_oauth_enabled: true

