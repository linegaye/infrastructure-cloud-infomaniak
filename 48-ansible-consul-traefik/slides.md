%title: Infra Cloud Infomaniak
%author: xavki


██╗███╗   ██╗███████╗██████╗  █████╗      ██████╗██╗      ██████╗ ██╗   ██╗██████╗ 
██║████╗  ██║██╔════╝██╔══██╗██╔══██╗    ██╔════╝██║     ██╔═══██╗██║   ██║██╔══██╗
██║██╔██╗ ██║█████╗  ██████╔╝███████║    ██║     ██║     ██║   ██║██║   ██║██║  ██║
██║██║╚██╗██║██╔══╝  ██╔══██╗██╔══██║    ██║     ██║     ██║   ██║██║   ██║██║  ██║
██║██║ ╚████║██║     ██║  ██║██║  ██║    ╚██████╗███████╗╚██████╔╝╚██████╔╝██████╔╝
╚═╝╚═╝  ╚═══╝╚═╝     ╚═╝  ╚═╝╚═╝  ╚═╝     ╚═════╝╚══════╝ ╚═════╝  ╚═════╝ ╚═════╝ 

-----------------------------------------------------------------------------------------------------------                                       

# Traefik & Consul

<br>

Add consul services

```
    - consul/consul_services
  vars:
    consul_services:
      - { name: "traefik_dashboard" }
      - { 
        name: "traefik_metrics",
        type: "http",
        target: "http://127.0.0.1:9200/metrics",
        interval: "10s",
        port: 9200
        }
```

-----------------------------------------------------------------------------------------------------------                                       

# Traefik & Consul

<br>

Add consul catalog to treafik.toml

```
[providers.consulCatalog]
  cache = false
  watch = true
  exposedByDefault=false
  [providers.consulCatalog.endpoint]
    datacenter = "xavki"
    address = "{{ traefik_consul_catalog }}"
```

-----------------------------------------------------------------------------------------------------------                                       

# Traefik & Consul

<br>

Add consul catalog to treafik.toml

```
- { 
  name: "grafana", 
  type: "http", 
  target: "http://127.0.0.1:3000", 
  interval: "10s", 
  port: 3000,
  tags: [
      "traefik.enable=true",
      "traefik.http.routers.router-grafana.entrypoints=http,https",
      "traefik.http.routers.router-grafana.rule=Host(`g.xavki.fr`)",
      "traefik.http.routers.router-grafana.service=grafana",
      "traefik.http.routers.router-grafana.middlewares=auth@file,to_https@file,secure_headers@file",
      "traefik.http.routers.router-grafana.tls.certresolver=certs_gen"
    ] }
```

-----------------------------------------------------------------------------------------------------------                                       

# Traefik & Consul

<br>

Add consul catalog to treafik.toml

```
- { 
    name: "vmetrics",
    type: "http",
    target: "http://127.0.0.1:8428",
    interval: "10s",
    port: 8428,
    tags: [
        "traefik.enable=true",
        "traefik.http.routers.router-vmetrics.entrypoints=http,https",
        "traefik.http.routers.router-vmetrics.rule=Host(`v.xavki.fr`)",
        "traefik.http.routers.router-vmetrics.service=vmetrics",
        "traefik.http.routers.router-vmetrics.middlewares=auth@file,to_https@file,secure_headers@file",
        "traefik.http.routers.router-vmetrics.tls.certresolver=certs_gen"
      ]
  }