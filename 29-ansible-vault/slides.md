%title: Infra Cloud Infomaniak
%author: xavki


██╗███╗   ██╗███████╗██████╗  █████╗      ██████╗██╗      ██████╗ ██╗   ██╗██████╗ 
██║████╗  ██║██╔════╝██╔══██╗██╔══██╗    ██╔════╝██║     ██╔═══██╗██║   ██║██╔══██╗
██║██╔██╗ ██║█████╗  ██████╔╝███████║    ██║     ██║     ██║   ██║██║   ██║██║  ██║
██║██║╚██╗██║██╔══╝  ██╔══██╗██╔══██║    ██║     ██║     ██║   ██║██║   ██║██║  ██║
██║██║ ╚████║██║     ██║  ██║██║  ██║    ╚██████╗███████╗╚██████╔╝╚██████╔╝██████╔╝
╚═╝╚═╝  ╚═══╝╚═╝     ╚═╝  ╚═╝╚═╝  ╚═╝     ╚═════╝╚══════╝ ╚═════╝  ╚═════╝ ╚═════╝ 

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Vault

<br>

Ansible Vault allows 2 methods to encrypt secrets :

  * an entire file :
      * useful when you have multiple secrets in a same file
      * not easy to search with grep to find some key variables

  * some values in a file with dedicated blocks :
      * need to do an encrypt method for each secret
      * you could search by variable name

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Vault

<br>

You could combine these methodes :

    * create an intermediate variable to store secret

    * push it into a vautl file

    * encrypt the entire file

    * use variable with the associate intermediate variable

More complex but useful

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Vault

<br>

Commands that you need to know :

```
ansible-vault encrypt vault.yml
ansible-vault edit vault.yml
ansible-vault decrypt vault.yml # avoid this methode
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Vault

<br>

Optionals :

```
ansible-vault create test.yml
ansible-vault view test.yml
ansible-vault encrypt_string 'mySecret' --name 'secretKey'
```

```
ansible playbook ... --ask-vault ...
ansible-playbook ... --vault-password-file ./.vault.txt ...
export ANSIBLE_VAULT_PASSWORD_FILE=./.vault.txt
ANSIBLE_VAULT_PASSWORD_FILE=./.vault.txt ansible-playbook ...
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Vault

<br>

Example :

```
    vault_xpestel_password: !vault |
      $ANSIBLE_VAULT;1.1;AES256
      33316639303364633966356435

    password: "{{ vault_xpestel_password }}"
```

```
ANSIBLE_VAULT_PASSWORD_FILE=~/.vault_password.yml"
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Vault

<br>

Refacto :

```
variable "ANSIBLE_ENV_VARS" {
  type = string
  default = "ANSIBLE_CONFIG=../../ansible/ansible.cfg ANSIBLE_VAULT_PASSWORD_FILE=~/.vault_password.yml"
}
variable "ANSIBLE_COMMAND" {
  type = string
  default = "ansible-playbook -u"
}
variable "ANSIBLE_OPTIONS" {
  type = string
  default = "-i ../../ansible/openstack.yml --private-key ~/.ssh/info"
}
```

-----------------------------------------------------------------------------------------------------------                                       

# Ansible - Vault

<br>

Finaly :

```
${var.ANSIBLE_ENV_VARS} ${var.ANSIBLE_COMMAND} ${var.default_user} ${var.ANSIBLE_OPTIONS} -e users_default_account=${var.default_user} ../../ansible/infrastructure_base.yml;
${var.ANSIBLE_ENV_VARS} ${var.ANSIBLE_COMMAND} ${var.default_user} ${var.ANSIBLE_OPTIONS}  ../../ansible/infrastructure_consul.yml;
```