%title: Infra Cloud Infomaniak
%author: xavki


██████╗  ██████╗ ███████╗████████╗ ██████╗ ██████╗ ███████╗███████╗ ██████╗ ██╗     
██╔══██╗██╔═══██╗██╔════╝╚══██╔══╝██╔════╝ ██╔══██╗██╔════╝██╔════╝██╔═══██╗██║     
██████╔╝██║   ██║███████╗   ██║   ██║  ███╗██████╔╝█████╗  ███████╗██║   ██║██║     
██╔═══╝ ██║   ██║╚════██║   ██║   ██║   ██║██╔══██╗██╔══╝  ╚════██║██║▄▄ ██║██║     
██║     ╚██████╔╝███████║   ██║   ╚██████╔╝██║  ██║███████╗███████║╚██████╔╝███████╗
╚═╝      ╚═════╝ ╚══════╝   ╚═╝    ╚═════╝ ╚═╝  ╚═╝╚══════╝╚══════╝ ╚══▀▀═╝ ╚══════╝



-----------------------------------------------------------------------------------------------------------

# Postgresql : WAL-G - archive log on S3 bucket

<br>

Change consul to have one member

Push loki on monitoring instance

```
  instance_flavor_name        = "a2-ram4-disk20-perf1"

  instance_volumes_count = 2 
```

```
  hosts: meta-app_monitoring

  loki_dir_data: "/data_loki/loki"
  volumes_disks:
    - {disk: '/dev/sdc',path: '/data_loki', owner: "loki"}

  vmagent_dir_disk_data: "/data_vmetrics"
  vmetrics_dir_disk_data: "/data_vmetrics"
```

Upgrade dependencies

-----------------------------------------------------------------------------------------------------------

# Postgresql : WAL-G - archive log on S3 bucket

<br>

Some variables

```
postgresql_replication_wal_g_enabled: false
postgresql_replication_wal_g_version: 3.0.0
postgresql_replication_wal_g_binary_path: /usr/local/bin
postgresql_replication_wal_g_s3_key: ""
postgresql_replication_wal_g_s3_secret_key: ""
postgresql_replication_wal_g_s3_endpoint: ""
postgresql_replication_wal_g_s3_bucket: ""
```

Create a container (bucket)

```
aws --endpoint-url=https://s3.pub1.infomaniak.cloud s3api create-bucket --bucket postgresql
```


-----------------------------------------------------------------------------------------------------------


# Postgresql : WAL-G - archive log on S3 bucket

<br>

Add wal-g dedicated tasks file

```
- name: include wal_g if needed
  include_tasks: wal_g.yml
  when: postgresql_replication_wal_g_enabled and postgresql_replication_primary == ansible_hostname

- name: install repmgr
```

-----------------------------------------------------------------------------------------------------------

# Postgresql : WAL-G - archive log on S3 bucket

<br>

Install wal-g directory

```
- name: install wal-g directory
  file:
    path: /etc/wal-g
    state: directory
    owner: postgres
    group: postgres
    mode: 0750
  when: postgresql_replication_wal_g_enabled
```

-----------------------------------------------------------------------------------------------------------

# Postgresql : WAL-G - archive log on S3 bucket

<br>

Add the configuration file

```
- name: add walg.json credentials
  template:
    src: walg.json.j2
    dest: "/etc/wal-g/walg.json"
    mode: 0640
    owner: postgres
    group: postgres
  notify: restart_postgresql
  when: postgresql_replication_wal_g_enabled
```

-----------------------------------------------------------------------------------------------------------

# Postgresql : WAL-G - archive log on S3 bucket

<br>

Check if wal-g is installed

```
- name: check if wal-g exists
  ansible.builtin.stat:
    path: "{{ postgresql_replication_wal_g_binary_path }}/wal-g"
  register: __wal_g_exists
  when: postgresql_replication_wal_g_enabled
```

-----------------------------------------------------------------------------------------------------------

# Postgresql : WAL-G - archive log on S3 bucket

<br>

Install the binary if needed

```
- name: install wal-g binary
  ansible.builtin.unarchive:
    src: "https://github.com/wal-g/wal-g/releases/download/v{{ postgresql_replication_wal_g_version }}/wal-g-pg-ubuntu-20.04-amd64.tar.gz"  
    dest: "/tmp/"
    remote_src: yes
    mode: 0750
    owner: postgres
    group: postgres
  when: postgresql_replication_wal_g_enabled and __wal_g_exists.stat.exists == False
```

-----------------------------------------------------------------------------------------------------------

# Postgresql : WAL-G - archive log on S3 bucket

<br>

Install the binary if needed

```
- name: move the binary to the final destination
  copy:
    src: "/tmp/wal-g-pg-ubuntu-20.04-amd64"
    dest: "{{ postgresql_replication_wal_g_binary_path }}/wal-g"
    mode: 0750
    remote_src: yes
    owner: "postgres"
    group: "postgres"
  when: postgresql_replication_wal_g_enabled and __wal_g_exists.stat.exists == False
```

-----------------------------------------------------------------------------------------------------------

# Postgresql : WAL-G - archive log on S3 bucket

<br>

Install the binary if needed

```
- name: clean
  file:
    path: "/tmp/wal-g-pg-ubuntu-20.04-amd64"
    state: absent
  when: postgresql_replication_wal_g_enabled and __wal_g_exists.stat.exists == False
```

-----------------------------------------------------------------------------------------------------------

# Postgresql : WAL-G - archive log on S3 bucket

<br>

Add replication.conf.j2

```
{% if postgresql_replication_wal_g_enabled and postgresql_replication_primary == ansible_hostname %} 
archive_command = '/usr/local/bin/wal-g --config /etc/wal-g/walg.json wal-push %p'
restore_command = '/usr/local/bin/wal-g --config /etc/wal-g/walg.json wal-fetch %f %p'
{% else %}
archive_command = '/bin/true'
{% endif %}
```

-----------------------------------------------------------------------------------------------------------

# Postgresql : WAL-G - archive log on S3 bucket

<br>

Add walg.json.j2

```
{
"WALE_S3_PREFIX": "{{ postgresql_replication_wal_g_s3_bucket }}",
"AWS_ACCESS_KEY_ID": "{{ postgresql_replication_wal_g_s3_key }}",
"AWS_ENDPOINT": "{{ postgresql_replication_wal_g_s3_endpoint }}",
"AWS_S3_FORCE_PATH_STYLE": "true",
"AWS_REGION": "us-east-1",
"AWS_SECRET_ACCESS_KEY": "{{ postgresql_replication_wal_g_s3_secret_key }}"
}
```

-----------------------------------------------------------------------------------------------------------

# Postgresql : WAL-G - archive log on S3 bucket

<br>

Our new playbook part

```
postgresql_replication_wal_g_enabled: true
postgresql_replication_wal_g_s3_key: "dd043eb53a3d47a58efd274dfd3438ea"
postgresql_replication_wal_g_s3_secret_key: "{{ vault_backups_consul_secret_key }}"
postgresql_replication_wal_g_s3_endpoint: "https://s3.pub1.infomaniak.cloud/"
postgresql_replication_wal_g_s3_bucket: "s3://wal-g"
```
