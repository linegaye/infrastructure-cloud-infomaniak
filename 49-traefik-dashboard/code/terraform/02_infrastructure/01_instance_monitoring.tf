
module "monitoring" {
  source                      = "../modules/instance"
  instance_count              = 1
  instance_name               = "monitoring"
  instance_key_pair           = "default_key"
  instance_security_groups    = ["consul", "ssh-internal", "all_internal", "node_exporter"]
  instance_network_internal   = var.network_internal_dev
  instance_ssh_key = var.ssh_public_key_default_user
  instance_default_user = var.default_user
  instance_volumes_count = 1  
  metadatas                   = {
    environment          = "dev",
    app         = "monitoring"
  }
}
      